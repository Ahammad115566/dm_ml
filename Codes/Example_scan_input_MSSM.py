VarMax =[] 
VarMin =[] 
VarLabel =[]
VarNum =[]
ConstMax =[]
ConstMin =[]
ConstLabel =[]
ConstNum =[]
ConstResNum =[]
ResLabel = []
ResNum = []
ResOpt = []
proc=[]
GridVar=[]
GridNum=[]
######################################
# Please do not change the above tags#
######################################
MODE = 1                   #1->spHeno , 2 cpsuperh,
npoints =150000       #Number of points you want to scan over
N_Cores = 45            #Number of cores to be used 
#####################################################
pathS ='/disk3/hammad/work/DM/scanner/SPheno-4.0.3' #Path to SPheno directory betweenthe quotes
Lesh ='LesHouches.in.MSSM_low_2'     
SPHENOMODEL ='MSSM'                   # Model Name as in spheno bin directory, like SPhenoMSSM, SPheno BLSSM, etc
#################################

VarMin.append(1.00000E+02)     # define the minimum value of varible
VarMax.append(4.0000E+02)     # define the maximumvalue of varible
VarLabel.append('# M1input')         # define the label of varible from Le
VarNum.append('1')              # Leshouches number of variable in Leshouches file

VarMin.append(1.000000E+02)  #2
VarMax.append(4.400000E+02)
VarLabel.append('# M2input')
VarNum.append('2')

VarMin.append(4.400000E+02)   #3
VarMax.append(4.000000E+03)
VarLabel.append('# Muinput')
VarNum.append('23')


VarMin.append(2.250000E+06)  #4
VarMax.append(2.250000E+06)
VarLabel.append('# mA2input')
VarNum.append('24')


VarMin.append(2.000000E+00)  #5
VarMax.append(6.000000E+01)
VarLabel.append('# TanBeta')
VarNum.append('25')

VarMin.append(1.000000E+04)  #6
VarMax.append(2.250000E+06)
VarLabel.append('# Real(ml2(1,1),dp)')
VarNum.append('  1  1 ')


VarMin.append(1.000000E+04)  #7
VarMax.append(2.250000E+06)
VarLabel.append('# Real(ml2(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+04) #8
VarMax.append(2.250000E+06)
VarLabel.append('# Real(ml2(3,3),dp)')
VarNum.append('  3  3 ')


VarMin.append(1.000000E+04)  #9
VarMax.append(2.250000E+06) 
VarLabel.append('# Real(me2(1,1),dp)')
VarNum.append('  1  1 ')


VarMin.append(1.000000E+04) #10
VarMax.append(2.250000E+06)
VarLabel.append('# Real(me2(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+04) #11
VarMax.append(2.250000E+06)
VarLabel.append('# Real(me2(3,3),dp)')
VarNum.append('  3  3 ')

###########################
##End of scanning parameters in this project##
###########################
VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(mQ2(1,1),dp)')
VarNum.append('  1  1 ')

VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(mQ2(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(mQ2(3,3),dp)')
VarNum.append('  3  3 ')


VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(md2(1,1),dp)')
VarNum.append('  1  1 ')


VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(md2(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(md2(3,3),dp)')
VarNum.append('  3  3 ')


VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(mu2(1,1),dp)')
VarNum.append('  1  1 ')


VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(mu2(2,2),dp)')
VarNum.append('  2  2 ')

VarMin.append(1.000000E+06)
VarMax.append(25.000000E+06)
VarLabel.append('# Real(mu2(3,3),dp)')
VarNum.append('  3  3 ')



VarMin.append(1.000000E+00)
VarMax.append(1.000000E+00)
VarLabel.append('# Real(Td(1,1),dp)')
VarNum.append('  1  1 ')


VarMin.append(1.000000E+00)
VarMax.append(1.000000E+00)
VarLabel.append('# Real(Td(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+00)
VarMax.append(1.000000E+02)
VarLabel.append('# Real(Td(3,3),dp)')
VarNum.append('  3  3 ')

VarMin.append(1.000000E+00)
VarMax.append(1.000000E+00)
VarLabel.append('# Real(Te(1,1),dp)')
VarNum.append('  1  1 ')


VarMin.append(1.000000E+00)
VarMax.append(1.000000E+00)
VarLabel.append('# Real(Te(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+00)
VarMax.append(1.000000E+02)
VarLabel.append('# Real(Te(3,3),dp)')
VarNum.append('  3  3 ')



VarMin.append(1.000000E+00)
VarMax.append(1.000000E+00)
VarLabel.append('# Real(Tu(1,1),dp)')
VarNum.append('  1  1 ')

VarMin.append(1.000000E+00)
VarMax.append(1.000000E+00)
VarLabel.append('# Real(Tu(2,2),dp)')
VarNum.append('  2  2 ')


VarMin.append(1.000000E+00)
VarMax.append(1.000000E+02)
VarLabel.append('# Real(Tu(3,3),dp)')
VarNum.append('  3  3 ')



TotVarScanned = 11  # Total number of variables scanned. 

#################### Grid ###########################
GridVar.append('1 * 1 <  2  <  1 * 1.1')  ## This mean the scanned variable number 7 will sampled in ranges from variable number 6 *1 to variables number 9 multiplied by 10
GridNum.append('1')  ## 2 for matrices and 1 for masses.

GridVar.append('1 * 1.1 <  3  <  1 * 10') 
GridNum.append('1')

GridVar.append('6 * 1 <  7  <  6 * 1') 
GridNum.append('2')

GridVar.append('6 * 1 <  8  <  6 * 1') 
GridNum.append('2')

GridVar.append('6 * 1 <  9 <  6 * 1') 
GridNum.append('2')

GridVar.append('6 * 1 <  10  <  6 * 1') 
GridNum.append('2')

GridVar.append('6 * 1 <  11 <  6 * 1') 
GridNum.append('2')


GridVariables = 7
###############################################################################
# Here to define the constraint spectrum you want out of SPheno               #
# TotConstScanned = 0   -----> no constraints will apply and                  #
#                              all spectrum files with out selection          #
###############################################################################
ConstMin.append(122)   # define the minimum value of constraint variable  
ConstMax.append(128)   # define the maximum value of constraint variable
ConstLabel.append('# hh_1') # define the label of constraint from Leshouches file 
ConstNum.append('25') # Leshouches number of constaint variable in Leshouches file 
ConstResNum.append('1')   # 1 for masses, 2 for matrices ( like yukawa & Sparticles mass matrix etc ), 0 for BR values 

ConstMin.append(1000021)    
ConstMax.append(1000023)   
ConstLabel.append('   # LSP ') 
ConstNum.append(' 1 ') 
ConstResNum.append('1')   

ConstMin.append(1.000000E-12)   
ConstMax.append(100)   
ConstLabel.append('# Chi_2') 
ConstNum.append('Decay') ## if this set to 'Decay' the code will constrain the total decay width of the given particle
ConstResNum.append('1')   


ConstMin.append(1.000000E-12)   
ConstMax.append(100)   
ConstLabel.append('# Cha_1') 
ConstNum.append('Decay') ## if this set to 'Decay' the code will constrain the total decay width of the given particle
ConstResNum.append('1') 


TotConstScanned = 4 # Total number of constraint variables scanned.

########################## Higgs Bounds #####################################
HiggsBounds =1#### if 1 ---> switch it on. if 0 swithch off 
HBPath = '/disk3/hammad/work/DM/scanner/HiggsBounds-4.3.1' ## Full path to Higgs bounds
NH     = '3'       # Number of neutral Higgs bosons 
NCH    = '1'       # Number of charged Higgs bosons 
ExcludeHB = 0      # if 1 --> remove the points excluded by HB. If 0 ---> keep the excluded points 
######################## Higgs Signals #######################################
HiggsSignal = 0 #### if 1 ---> switch it on. if 0 swithch off 
HSPath =  '/disk3/hammad/work/DM/scanner/HiggsSignals-1.4.0'  ## Full path to Higgs bounds
PATHMHUNCERTINITY = '/disk3/hammad/work/DM/scanner/MHall_uncertainties.dat' ## mass_uncertinity file needed by higgssignals
ExcludeHS = 100     # put the upper value of chi square total ..  if 0 do not remove any spec

######################## Micro Omegas##########################################

MicoOmegas = 1  #### if 1 ---> switch it on. if 0 swithch off 
MicoOmegaspath = '/disk3/hammad/work/DM/scanner/micromegas_5.2.1/MSSM_new'  ## Full path to the excutable main file in the MicroOmegas directory
######################### MadGraph ########################################

MadGraph = 1

proc.append('''import model MSSM_SLHA2
define n = n2 
define c11 = x1+ x1-
define c22 = x2+ x2-
define ca = c11 c22
generate p p > c11 n / h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~
''' )   


proc.append('''import model MSSM_SLHA2
define n = n2 n3 n4
define c11 = x1+ x1-
define c22 = x2+ x2-
define ca = c11 c22
define w = w+ w-
define l = l+ l-
define e = e+ e-
define mu = mu+ mu-
define vee = ve ve~
define vmm = vm vm~
define vv = vee vmm
define q = u c d s b
define qb = u~ c~ d~ s~ b~
generate p p > ca n / h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~, n > n1 e+ e- / a  h01 h2 h3 h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ el- mul- ta1- er- mur- ta2- el+ mul+ ta1+ er+ mur+ ta2+ h- h+ sve svm svt sve~ svm~ svt~, ca > n1 e vee / a z h01 h2 h3 h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ el- mul- ta1- er- mur- ta2- el+ mul+ ta1+ er+ mur+ ta2+ h- h+ sve svm svt sve~ svm~ svt~
''' )   



proc.append('''import model MSSM_SLHA2
define n = n2 
define c11 = x1+ x1-
generate p p > c11 n / h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~
''' )   

proc.append('''import model MSSM_SLHA2
define n = n2 n3 n4
define c11 = x1+ x1-
define c22 = x2+ x2-
define ca = c11 c22
generate p p > ca n / h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~
''' )    


proc.append('''import model MSSM_SLHA2
define n = n2 
define c11 = x1+ x1-
define c22 = x2+ x2-
define ca = c11 c22
define w = w+ w-
define l = l+ l-
define e = e+ e-
define mu = mu+ mu-
define vee = ve ve~
define vmm = vm vm~
define vv = vee vmm
define q = u c d s b
define qb = u~ c~ d~ s~ b~
generate p p > c11 n / h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~, n > n1 e+ e- / a  h01 h2 h3 h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ el- mul- ta1- er- mur- ta2- el+ mul+ ta1+ er+ mur+ ta2+ h- h+ sve svm svt sve~ svm~ svt~, ca > n1 e vee / a z h01 h2 h3 h+ h- ul cl t1 ur cr t2 dl sl b1 dr sr b2 ul~ cl~ t1~ ur~ cr~ t2~ dl~ sl~ b1~ dr~ sr~ b2~ el- mul- ta1- er- mur- ta2- el+ mul+ ta1+ er+ mur+ ta2+ h- h+ sve svm svt sve~ svm~ svt~
''' )    


 
  

madgraph_path = '/disk3/hammad/work/DM/scanner/MG5_aMC_v3_4_2/' 
RunCard_path = '/disk3/hammad/work/DM/scanner/MG5_aMC_v3_4_0_RC3/'  

